# Recommended Reading

## Leadership

- [Principles: Life And Work](https://www.amazon.com/Principles-Life-Work-Ray-Dalio/dp/1501124021/ref=sr_1_3?keywords=Principles%3A+Life+And+Work&qid=1553045313&s=gateway&sr=8-3) by _Ray Dalio_
- [Integrity: The Courage To Meet The Demands Of Reality](https://www.amazon.com/Integrity-Courage-Meet-Demands-Reality/dp/006084969X/ref=sr_1_2?crid=2KT5SFH744BQB&keywords=integrity+henry+cloud&qid=1553045611&s=gateway&sprefix=Integrity%2Caps%2C139&sr=8-2) by _Henry Cloud_
- [Great By Choice: Uncertainty, Chaos, and Luck - Why some Thrive Despite Them All](https://www.amazon.com/Great-Choice-Uncertainty-Luck-Why-Despite/dp/0062120999/ref=sr_1_1?keywords=great+by+choice&qid=1553045661&s=gateway&sr=8-1) by _Jim Collins_
- [How The Mighty Fall: And Why Some Companies Never Give In](https://www.amazon.com/How-Mighty-Fall-Companies-Never/dp/0977326411/ref=sr_1_1?crid=XZD8ZXGPLEDY&keywords=how+the+mighty+fall+jim+collins&qid=1553045684&s=gateway&sprefix=how+the+mighty+%2Caps%2C140&sr=8-1) by _Jim Collins_
- [Good To Great: Why Some Companies Make the Leap And Others Don’t](https://www.amazon.com/Good-Great-Some-Companies-Others/dp/0066620996/ref=sr_1_1?keywords=Good+To+Great&qid=1553045710&s=gateway&sr=8-1) by _Jim Collins_
- [Start With Why: How Great Leaders Inspire Everyone To Take Action](https://www.amazon.com/Start-Why-Leaders-Inspire-Everyone/dp/1591846447/ref=sr_1_2?keywords=Start+With+Why&qid=1553045722&s=gateway&sr=8-2) by _Simon Sinek_
- [Essentialism: The disciplined pursuit of less](https://www.amazon.com/Essentialism-Disciplined-Pursuit-Greg-McKeown/dp/0804137382) by _Greg McKeown_
- [Multipliers: How the best leaders make everyone smarter](https://www.amazon.com/Multipliers-Revised-Updated-Leaders-Everyone/dp/0062663070/ref=sr_1_3?dchild=1&keywords=Multipliers&qid=1627996473&s=books&sr=1-3) by _Liz Wiseman_


## People, Business, & Culture

- [Peopleware: Productive Projects & Teams](https://www.amazon.com/Peopleware-Productive-Projects-Teams-3rd/dp/0321934113/ref=sr_1_1?keywords=peopleware&qid=1553045768&s=gateway&sr=8-1) by _Tom Demarco_
- [Slack: Getting Past Burnout, Busywork, and the Myth of Total Efficiency](https://www.amazon.com/Slack-Getting-Burnout-Busywork-Efficiency/dp/0767907698/ref=sr_1_1?crid=2UYPFABCE6C2J&keywords=slack+getting+past+burnout&qid=1553045801&s=gateway&sprefix=Slack+getting%2Caps%2C137&sr=8-1) by _Tom Demarco_
- [Rework](https://www.amazon.com/Rework-Jason-Fried/dp/0307463745/ref=sr_1_1?keywords=Rework&qid=1553045817&s=gateway&sr=8-1) by _Jason Fried_ and _DHH_
- [Remote: Office Not Required](https://www.amazon.com/Remote-Office-Required-Jason-Fried/dp/0804137501/ref=sr_1_1?crid=3QKFGAJSR7ZKE&keywords=remote+office+not+required&qid=1553045845&s=gateway&sprefix=Remote+office%2Caps%2C139&sr=8-1) by _Jason Fried_ and _DHH_
- [To Sell Is Human: The Surprising Truth About Moving Others](https://www.amazon.com/Sell-Human-Surprising-Moving-Others/dp/1594631905/ref=sr_1_1?keywords=to+sell+is+human&qid=1553045868&s=gateway&sr=8-1) by _Dan Pink_
- [The Intelligent Investor: The Definitive Book on Value Investing](https://www.amazon.com/Intelligent-Investor-Definitive-Investing-Essentials/dp/0060555661/ref=sr_1_1?crid=G24G5SI5W04L&keywords=the+intelligent+investor+by+benjamin+graham&qid=1553046034&s=gateway&sprefix=the+intelligent%2Caps%2C144&sr=8-1) by _Benjamin Graham_
- [It Doesn’t Have To Be Crazy At Work](https://www.amazon.com/Doesnt-Have-Be-Crazy-Work/dp/0062874780/ref=sr_1_1?crid=2WP0TJHX5PT0K&keywords=it+doesnt+have+to+be+crazy+at+work&qid=1553046394&s=gateway&sprefix=it+doesnt+%2Caps%2C145&sr=8-1) by _Jason Fried_ and _DHH_
- [Workplace Management](https://www.amazon.com/Taiichi-Ohnos-Workplace-Management-Birthday/dp/0071808019/ref=sr_1_1?dchild=1&keywords=Workplace+management+taiichi+ohno&qid=1627996504&s=books&sr=1-1) by _Taiichi Ohno_
- [Crucial Conversations: Tools for talking when stakes are high](https://www.amazon.com/Crucial-Conversations-Talking-Stakes-Second/dp/0071771328/ref=sr_1_3?dchild=1&keywords=crucial+conversations&qid=1627996526&s=books&sr=1-3) by _Patterson, Grenny, McMillan, Switzler_
- [Invisible Solutions: 25 Lenses that reframe and help solve difficult business problems](https://www.amazon.com/Invisible-Solutions-Difficult-Business-Problems/dp/164543186X/ref=sr_1_1?dchild=1&keywords=invisible+solutions&qid=1627996550&s=books&sr=1-1) by _Stephen M. Shapiro_
- [High Output Management](https://www.amazon.com/High-Output-Management-Andrew-Grove/dp/0679762884/ref=sr_1_3?dchild=1&keywords=high+output+management&qid=1627996672&s=books&sr=1-3) by _Andrew S. Grove_
- [The Dip: A Little Book That Teaches You When to Quit (And When To Stick)](https://www.amazon.com/Dip-Little-Book-Teaches-Stick/dp/1591841666/ref=sr_1_2?dchild=1&keywords=the+dip&qid=1627996766&s=books&sr=1-2) by _Seth Godin_


## Product Management

- [Inspired: How to create tech products customers love](https://www.amazon.com/INSPIRED-Create-Tech-Products-Customers/dp/1119387507/ref=sr_1_3?dchild=1&keywords=inspired&qid=1627996563&s=books&sr=1-3) by _Marty Cagan_
- [Empowered: Ordinary people, extraordinary products](https://www.amazon.com/EMPOWERED-Ordinary-Extraordinary-Products-Silicon/dp/111969129X/ref=sr_1_3?dchild=1&keywords=empowered&qid=1627996602&s=books&sr=1-3) by _Marty Cagan_
- [The Build Trap: How effective product management creates real value](https://www.amazon.com/Escaping-Build-Trap-Effective-Management/dp/149197379X/ref=sr_1_3?dchild=1&keywords=the+build+trap&qid=1627996691&s=books&sr=1-3) by _Melissa Perri_
- [Blue Ocean Strategy: How to create uncontested market space and make the competition irrelevant](https://www.amazon.com/Blue-Ocean-Strategy-Expanded-Uncontested/dp/1625274491/ref=sr_1_1?dchild=1&keywords=blue+ocean+strategy&qid=1627996708&s=books&sr=1-1) by _W. Chan Kim_ and _Renee Mauborgne_
- [Product Roadmaps Relaunched: How to Set Direction while Embracing Uncertainty](https://www.amazon.com/Product-Roadmaps-Relaunched-Direction-Uncertainty/dp/149197172X/ref=sr_1_3?dchild=1&keywords=product+roadmaps+relaunched&qid=1627996738&s=books&sr=1-3) by _C. Todd Lombardo_
- [Crossing the Chasm: Marketing and selling disruptive products to mainstream customers](https://www.amazon.com/Crossing-Chasm-3rd-Disruptive-Mainstream/dp/0062292986/ref=sr_1_3?dchild=1&keywords=crossing+the+chasm&qid=1627996802&s=books&sr=1-3) by _Geoffrey A. Moore_
- [When Coffee & Kale Compete](https://www.amazon.com/When-Coffee-Kale-Compete-products/dp/1534873066/ref=sr_1_3?dchild=1&keywords=when+coffee+and+kale+compete&qid=1627996820&s=books&sr=1-3) by _Alan Klement_
- [Project to Product](https://www.amazon.com/Project-Product-Survive-Disruption-Framework/dp/1942788398/ref=sr_1_3?dchild=1&keywords=project+to+product&qid=1627996839&s=books&sr=1-3) by _Mik Kersten_
- [Competing Against Luck: The story of innovation and customer choice](https://www.amazon.com/Competing-Against-Luck-Innovation-Customer/dp/0062435612/ref=sr_1_2?dchild=1&keywords=competing+against+luck&qid=1627996859&s=books&sr=1-2) by _Clayton M. Christensen_

## Process, Systems, & Tactics

- [Measure What Matters](https://www.amazon.com/Measure-What-Matters-Google-Foundation/dp/0525536221/ref=sr_1_2?dchild=1&keywords=measure+what+matters&qid=1627996879&s=books&sr=1-2) by _John Doerr_
- [Running Lean: Iterate From Plan A To A Plan That Works](https://www.amazon.com/Running-Lean-Iterate-Plan-Works/dp/1449305172/ref=sr_1_1?keywords=running+lean&qid=1553046080&s=gateway&sr=8-1) by _Ash Maurya_
- [Scaling Lean: Mastering The Key Metrics For Startup Growth](https://www.amazon.com/Scaling-Lean-Mastering-Metrics-Startup/dp/1101980524/ref=sr_1_1?keywords=scaling+lean&qid=1553046110&s=gateway&sr=8-1) by _Ash Maurya_
- [The Goal: A Process of Ongoing Improvement](https://www.amazon.com/Goal-Process-Ongoing-Improvement/dp/0884271951/ref=sr_1_1?crid=1QINJ1Q73IB5M&keywords=the+goal+by+eliyahu+goldratt&qid=1553046199&s=gateway&sprefix=The+Goal%2Caps%2C142&sr=8-1) by _Eliyahu M. Goldratt_
- [The Art of Agile Development: Pragmatic Guide To Agile Software Development](https://www.amazon.com/Art-Agile-Development-Pragmatic-Software/dp/0596527675/ref=sr_1_1?crid=2VE9P77HWOFAX&keywords=the+art+of+agile+development&qid=1553046238&s=gateway&sprefix=The+art+of+agile%2Caps%2C137&sr=8-1) by _James Shore_
- [Thinking in Systems: A Primer](https://www.amazon.com/Thinking-Systems-Donella-H-Meadows/dp/1603580557/ref=sr_1_1?keywords=thinking+in+systems&qid=1553046256&s=gateway&sr=8-1) by _Donella H. Meadows_
- [Toyota Production System: Beyond Large-Scale Production](https://www.amazon.com/Toyota-Production-System-Beyond-Large-Scale/dp/0915299143/ref=sr_1_1?crid=1FX3P5A62J0RF&keywords=toyota+production+system+beyond+large-scale+production&qid=1553046280&s=gateway&sprefix=Toyota+Product%2Caps%2C138&sr=8-1) by _Taiichi Ohno_
- [Lean UX: Designing Great Products With Agile Teams](https://www.amazon.com/Lean-UX-Designing-Great-Products/dp/1491953608/ref=sr_1_2?keywords=Lean+UX&qid=1553046300&s=gateway&sr=8-2) by _Jeff Gothelf_
- [The Pragmatic Programmer: From Journeyman To Master](https://www.amazon.com/Pragmatic-Programmer-Journeyman-Master/dp/020161622X/ref=sr_1_1?keywords=pragmatic+programmer&qid=1553046338&s=gateway&sr=8-1) by _Andrew Hunt_ and _Dave Thomas_
- [Clean Code: A Handbook of Agile Software Craftsmanship](https://www.amazon.com/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882/ref=sr_1_2?keywords=pragmatic+programmer&qid=1553046358&s=gateway&sr=8-2) by _Robert C. Martin_
- [Lean Analytics: Use Data To Build A Better Startup Faster](https://www.amazon.com/Lean-Analytics-Better-Startup-Faster/dp/1449335675/ref=sr_1_3?keywords=lean+analytics&qid=1553046126&s=gateway&sr=8-3) by _Alistair Croll_ and _Benjamin Yoskovitz_
- [Kanban: Successful evolutionary change for your technology business](https://www.amazon.com/Kanban-Successful-Evolutionary-Technology-Business/dp/0984521402/ref=sr_1_4?dchild=1&keywords=kanban&qid=1627996917&s=books&sr=1-4) by _David J. Anderson_
- [How To Make Sense Of Any Mess](https://www.amazon.com/How-Make-Sense-Any-Mess/dp/1500615994) by _Abby Covert_

